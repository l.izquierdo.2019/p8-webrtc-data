import asyncio
import time

from aiortc import RTCIceCandidate, RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.signaling import BYE, CopyAndPasteSignaling


async def consume_signaling(pc, signaling):
    while True:
        obj = await signaling.receive()

        if isinstance(obj, RTCSessionDescription):
            await pc.setRemoteDescription(obj)

            if obj.type == "offer":
                # send answer
                await pc.setLocalDescription(await pc.createAnswer())
                await signaling.send(pc.localDescription)
        elif obj is BYE:
            print("Exiting")
            break


time_start = None


def current_stamp():
    global time_start

    if time_start is None:
        time_start = time.time()
        return 0
    else:
        return int((time.time() - time_start) * 1000000)


async def run_offer(pc, signaling):
    await signaling.connect()

    channel = pc.createDataChannel("chat")
    print(f"channel({channel.label}) > created by local party")

    async def send_pings():
        while True:
            message = f"ping {current_stamp()}"
            print(f"channel({channel.label}) > {message}")
            channel.send(message)
            await asyncio.sleep(1)

    @channel.on("open")
    def on_open():
        asyncio.ensure_future(send_pings())

    @channel.on("message")
    def on_message(message):
        print(f"channel({channel.label}) > {message}")

        if isinstance(message, str) and message.startswith("pong"):
            elapsed_ms = (current_stamp() - int(message[5:])) / 1000
            print(" RTT %.2f ms" % elapsed_ms)

    # send offer
    await pc.setLocalDescription(await pc.createOffer())
    await signaling.send(pc.localDescription)

    await consume_signaling(pc, signaling)


if __name__ == "__main__":
    signaling = CopyAndPasteSignaling()
    pc = RTCPeerConnection()
    coro = run_offer(pc, signaling)

    # run event loop
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(coro)
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())
        loop.run_until_complete(signaling.close())
        #Guardar el mensaje SDP en un archivo
        with open("client_data.sdp", "w") as file:
            file.write(
                "v=0\r\no=- 3909200139 3909200139 IN IP4 0.0.0.0\r\ns=-\r\nt=0 0\r\na=group:BUNDLE 0\r\na=msid-semantic:WMS *\r\nm=application 34237 DTLS/SCTP 5000\r\nc=IN IP4 212.128.255.164\r\na=mid:0\r\na=sctpmap:5000 webrtc-datachannel 65535\r\na=max-message-size:65536\r\na=candidate:34a9ee9f7ef5eeee00164d70e53e91b8 1 udp 2130706431 212.128.255.164 34237 typ host\r\na=candidate:580b5f32035da7f14a30ea7a8d826c67 1 udp 2130706431 172.17.0.1 32985 typ host\r\na=candidate:0ad8db753210ce9554cf251a9e51e792 1 udp 1694498815 212.128.255.164 32985 typ srflx raddr 172.17.0.1 rport 32985\r\na=candidate:a3786762ddd8767410ff7abc48afa356 1 udp 1694498815 212.128.255.164 34237 typ srflx raddr 212.128.255.164 rport 34237\r\na=end-of-candidates\r\na=ice-ufrag:i1b3\r\na=ice-pwd:IQ4bue98K6aucl1ZWn3f6C\r\na=fingerprint:sha-256 A2:B6:66:E0:92:10:0E:C3:9D:A7:12:BD:3D:6F:B0:A5:49:D1:27:BB:02:E6:2E:0A:38:D7:B4:BD:6D:4B:23:A2\r\na=setup:actpass\r\n")
