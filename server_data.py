import asyncio

from aiortc import RTCIceCandidate, RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.signaling import BYE, CopyAndPasteSignaling


async def consume_signaling(pc, signaling):
    while True:
        obj = await signaling.receive()

        if isinstance(obj, RTCSessionDescription):
            await pc.setRemoteDescription(obj)

            if obj.type == "offer":
                # send answer
                await pc.setLocalDescription(await pc.createAnswer())
                await signaling.send(pc.localDescription)
        elif obj is BYE:
            print("Exiting")
            break


time_start = None


async def run_answer(pc, signaling):
    await signaling.connect()

    @pc.on("datachannel")
    def on_datachannel(channel):
        print(f"channel({channel.label}) > created by remote party")

        @channel.on("message")
        def on_message(message):

            print(f"channel({channel.label}) > {message}")

            if isinstance(message, str) and message.startswith("ping"):
                # reply
                message = f"pong{message[4:]}"
                print(f"channel({channel.label}) > {message}")
                channel.send(message)

    await consume_signaling(pc, signaling)


if __name__ == "__main__":
    signaling = CopyAndPasteSignaling()
    pc = RTCPeerConnection()
    coro = run_answer(pc, signaling)

    # run event loop
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(coro)
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())
        loop.run_until_complete(signaling.close())

    # Guardar el mensaje SDP en un archivo
    with open("server_data.sdp", "w") as file:
        file.write(
            "v=0\r\no=- 3909200224 3909200224 IN IP4 0.0.0.0\r\ns=-\r\nt=0 0\r\na=group:BUNDLE 0\r\na=msid-semantic:WMS *\r\nm=application 39013 DTLS/SCTP 5000\r\nc=IN IP4 212.128.255.164\r\na=mid:0\r\na=sctpmap:5000 webrtc-datachannel 65535\r\na=max-message-size:65536\r\na=candidate:34a9ee9f7ef5eeee00164d70e53e91b8 1 udp 2130706431 212.128.255.164 39013 typ host\r\na=candidate:580b5f32035da7f14a30ea7a8d826c67 1 udp 2130706431 172.17.0.1 48682 typ host\r\na=candidate:0ad8db753210ce9554cf251a9e51e792 1 udp 1694498815 212.128.255.164 48682 typ srflx raddr 172.17.0.1 rport 48682\r\na=candidate:a3786762ddd8767410ff7abc48afa356 1 udp 1694498815 212.128.255.164 39013 typ srflx raddr 212.128.255.164 rport 39013\r\na=end-of-candidates\r\na=ice-ufrag:NnAO\r\na=ice-pwd:rKOCMKxQts7iC5gfIFcyvw\r\na=fingerprint:sha-256 71:F8:2A:60:9E:11:8D:27:DD:0B:40:F6:3E:84:EA:D3:A5:CD:D9:43:2C:93:75:F6:9A:4E:C0:03:2A:92:AC:83\r\na=setup:active\r\n")
