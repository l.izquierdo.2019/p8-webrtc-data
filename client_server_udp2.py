import asyncio
import json
import socket
from aiortc import RTCPeerConnection, RTCSessionDescription

async def run():
    pc = RTCPeerConnection()

    # Registra con el servidor de señalización UDP
    send_udp_message("REGISTER SERVER", ("localhost", 12345))

    # Escuchar la oferta SDP
    while True:
        message = receive_udp_message()
        if message and message["type"] == "offer":
            await pc.setRemoteDescription(RTCSessionDescription(sdp=message["sdp"], type=message["type"]))
            answer = await pc.createAnswer()
            await pc.setLocalDescription(answer)
            send_udp_message(json.dumps({"sdp": answer.sdp, "type": answer.type}), ("localhost", 12345))
            break

    await asyncio.sleep(60)
    await pc.close()

def send_udp_message(message, server_address):
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        sock.sendto(message.encode(), server_address)

def receive_udp_message():
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        sock.bind(('', 0))  # Cambiar si es necesario
        data, _ = sock.recvfrom(1024)
        return json.loads(data.decode())

asyncio.run(run())
